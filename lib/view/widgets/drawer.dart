import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../controller/login.dart';
import '../icons/my_icons2.dart';
import '../pages/categories_page.dart';
import '../pages/login_page.dart';
import '../pages/transaction_page.dart';

class DrawerWitdget extends StatefulWidget {
  const DrawerWitdget({super.key});

  @override
  State<DrawerWitdget> createState() => _DrawerWitdgetState();
}

class _DrawerWitdgetState extends State<DrawerWitdget> {
  final _loginController = LoginController();
  final _pref = SharedPreferences.getInstance();
  String _name = "";
  String _email = "";

  @override
  void initState() {
    super.initState();

    _pref.then((pref) {
      setState(() {
        _name = pref.getString("name") ?? "N/A";
        _email = pref.getString("email") ?? "N/A";
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              color: Theme.of(context).appBarTheme.backgroundColor,
            ),
            child: _header(),
          ),
          ListTile(
            leading: const Icon(MyIcons2.moneyCheckAlt),
            title: const Text('Transacciones'),
            onTap: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => const TransactionPage()),
              );
            },
          ),
          ListTile(
            leading: const Icon(MyIcons2.chartBar),
            title: const Text('Reportes'),
            onTap: () {},
          ),
          ListTile(
            leading: const Icon(Icons.category),
            title: const Text('Categorias'),
            onTap: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => const CategoriesPage()),
              );
            },
          ),
          ListTile(
            leading: const Icon(Icons.settings),
            title: const Text('Ajustes'),
            onTap: () {},
          ),
          ListTile(
            leading: const Icon(Icons.exit_to_app),
            title: const Text('Cerrar Cesion'),
            onTap: () async {
              var nav = Navigator.of(context);

              //  Cerrar sesion en Auth de Firebase
              _loginController.logout();

              // Limpiar las preferences
              var pref = await _pref;
              pref.remove("uid");
              pref.remove("email");
              pref.remove("name");
              pref.remove("admin");

              // Volver a pagina de login
              nav.pushReplacement(MaterialPageRoute(
                builder: (context) => LoginPage(),
              ));
            },
          ),
        ],
      ),
    );
  }

  Widget _header() {
    //Consultar los datos de la cabecera
    const image = Icon(Icons.manage_accounts);

    return Row(
      children: [
        const CircleAvatar(
          radius: 30,
          child: image,
        ),
        const SizedBox(width: 8),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                _name,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                ),
              ),
              const SizedBox(height: 8),
              Text(
                _email,
                style: const TextStyle(
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

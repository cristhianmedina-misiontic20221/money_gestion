import 'package:flutter/material.dart';

enum SelectOption { ingreso, gasto }

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({super.key});

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  SelectOption? _select = SelectOption.ingreso;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Row(
          children: [
            Radio(
              value: SelectOption.ingreso,
              groupValue: _select,
              onChanged: (SelectOption? value) {
                setState(() {
                  _select = value;
                });
              },
            ),
            const Text("Ingreso"),
          ],
        ),
        Row(
          children: [
            Radio(
              value: SelectOption.gasto,
              groupValue: _select,
              onChanged: (SelectOption? value) {
                setState(() {
                  _select = value;
                });
              },
            ),
            const Text("Gasto"),
          ],
        ),
      ],
    );
  }
}

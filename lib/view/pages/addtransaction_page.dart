import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../controller/transaction.dart';
import '../../model/entity/transaction.dart';

class AddTransactionPage extends StatelessWidget {
  final String fecha = "dd/mm/yy";

  final _pref = SharedPreferences.getInstance();

  late final TransactionEntity _transaction;
  late final TransactionController _controller;

  AddTransactionPage({super.key}) {
    _transaction = TransactionEntity();
    _controller = TransactionController();
    _pref.then((pref) {
      _transaction.user = pref.getString("uid");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Agregar Transacción"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: _form(context),
        ),
      ),
    );
  }

  Widget _form(context) {
    final formkey = GlobalKey<FormState>();

    return Form(
      key: formkey,
      child: Column(
        children: [
          const SizedBox(height: 15),
          _fieldTitle(),
          const SizedBox(height: 10),
          _fieldValue(),
          const SizedBox(height: 10),
          _fieldSelect(),
          const SizedBox(height: 10),
          _fieldDate(fecha),
          const SizedBox(height: 10),
          _fielCategory(),
          const SizedBox(height: 10),
          _fieldDescription(),
          const SizedBox(height: 10),
          //_fieldAddPhoto(),
          const SizedBox(height: 10),
          _buttonSave(context, formkey),
        ],
      ),
    );
  }

  Widget _buttonSave(BuildContext context, GlobalKey<FormState> formkey) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            Theme.of(context).appBarTheme.backgroundColor, // Background color
      ),
      child: const Text("Guardar"),
      onPressed: () async {
        if (formkey.currentState!.validate()) {
          formkey.currentState!.save();

          //Guardar los datos en la BD
          try {
            final mess = ScaffoldMessenger.of(context);
            final nav = Navigator.of(context);

            await _controller.save(_transaction);

            mess.showSnackBar(
              const SnackBar(
                content: Text("Transaccion agreagada con exito"),
              ),
            );
            nav.pop();
          } catch (e) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text("Error: $e"),
              ),
            );
          }
        }
      },
    );
  }

  Widget _fieldTitle() {
    return TextFormField(
      //autofocus: true,
      maxLength: 20,
      keyboardType: TextInputType.name,
      decoration: const InputDecoration(
        //icon: Icon(Icons.title),
        border: OutlineInputBorder(),
        labelText: 'Titulo',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El campo es obligatorio";
        }
        return null;
      },
      onSaved: (value) {
        _transaction.title = value!;
      },
    );
  }

  Widget _fieldValue() {
    return TextFormField(
      initialValue: "0",
      textAlign: TextAlign.right,
      keyboardType: TextInputType.number,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Monto',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El campo es obligatorio";
        }
        if (int.parse(value) < 0) {
          return "El valor no es valido";
        }
        return null;
      },
      onSaved: (value) {
        _transaction.amount = int.tryParse(value!);
      },
    );
  }

  Widget _fieldSelect() {
    //return const MyStatefulWidget();
    var opciones = <String>["Ingreso", "Gasto"];
    var defectType = opciones[0];
    _transaction.type = defectType;
    return DropdownButtonFormField(
      value: defectType,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Tipo',
      ),
      items: opciones
          .map<DropdownMenuItem<String>>(
              (String value) => DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  ))
          .toList(),
      onChanged: (value) {
        _transaction.type = value;
      },
    );
  }

  Widget _fieldDate(String fecha) {
    return Row(
      children: [
        IconButton(
          icon: const Icon(Icons.date_range),
          onPressed: () {},
        ),
        Expanded(
          child: TextFormField(
            initialValue: fecha,
            keyboardType: TextInputType.datetime,
            decoration: const InputDecoration(
              //icon: Icon(Icons.title),
              border: OutlineInputBorder(),
              labelText: "Fecha",
            ),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return "El campo es obligatorio";
              }
              return null;
            },
            onSaved: (value) {
              _transaction.date = value!;
            },
          ),
        ),
      ],
    );
  }

  Widget _fielCategory() {
    var opciones = <String>["Servicios", "Sueldo", "Comida", "Entretenimiento"];
    var defectCategory = opciones[0];
    _transaction.category = defectCategory;
    return DropdownButtonFormField(
      value: defectCategory,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Categoria',
      ),
      items: opciones
          .map<DropdownMenuItem<String>>(
              (String value) => DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  ))
          .toList(),
      onChanged: (value) {
        _transaction.category = value;
      },
    );
  }

  Widget _fieldDescription() {
    return TextFormField(
      keyboardType: TextInputType.text,
      decoration: const InputDecoration(
        //icon: Icon(Icons.title),
        border: OutlineInputBorder(),
        labelText: 'Descripcion',
      ),
      onSaved: (value) {
        _transaction.description = value!;
      },
    );
  }

  Widget _fieldAddPhoto() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Divider(
          height: 20,
          thickness: 2,
        ),
        const Text(
          "Agregar Fotografia",
          style: TextStyle(
            fontSize: 14,
            color: Colors.grey,
          ),
          textAlign: TextAlign.center,
        ),
        IconButton(
          icon: const Icon(
            Icons.add_a_photo,
            size: 40,
          ),
          onPressed: () {},
        ),
      ],
    );
  }
}

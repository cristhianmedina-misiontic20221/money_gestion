import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../controller/transaction.dart';
import '../../model/entity/transaction.dart';
import '../widgets/drawer.dart';
import 'addtransaction_page.dart';
import 'edittransaction_page.dart';

class TransactionPage extends StatefulWidget {
  const TransactionPage({super.key});

  @override
  State<TransactionPage> createState() => _TransactionPageState();
}

class _TransactionPageState extends State<TransactionPage> {
  List<TransactionEntity> _lista = [];
  int total = 0;
  final _pref = SharedPreferences.getInstance();
  final _transactionController = TransactionController();

  @override
  void initState() {
    super.initState();
    _listTransaction();
  }

  String cuenta = "Noviembre/2022";
  bool isSearching = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          title: !isSearching ? _title("\$  ${total.toString()}") : _seach(),
          centerTitle: true,
          actions: <Widget>[
            isSearching
                ? IconButton(
                    icon: const Icon(Icons.cancel),
                    onPressed: () {
                      setState(() {
                        isSearching = false;
                      });
                    },
                  )
                : IconButton(
                    icon: const Icon(Icons.search),
                    onPressed: () {
                      setState(() {
                        isSearching = true;
                      });
                    },
                  ),
          ],
        ),
        drawer: const DrawerWitdget(),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: ListView.builder(
                  itemCount: _lista.length,
                  itemBuilder: (context, index) => ListTile(
                    title: Text(_lista[index].title!),
                    subtitle: Text(_lista[index].date!),
                    trailing: Text(
                      _lista[index].type == "Ingreso"
                          ? "+ ${_lista[index].amount!.toString()}"
                          : "- ${_lista[index].amount!.toString()}",
                      style: _lista[index].type == "Ingreso"
                          ? const TextStyle(color: Colors.green)
                          : const TextStyle(color: Colors.red),
                    ),
                    onTap: () {
                      //Ir a la ventana de cobro
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const EditTransactionPage(),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
          child: const Icon(Icons.add),
          onPressed: () async {
            await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AddTransactionPage(),
              ),
            );

            if (!mounted) return;
            _listTransaction();
          },
        ),
      ),
    );
  }

  Widget _title(String saldo) {
    return Column(
      children: [
        // Text(
        //   cuenta,
        //   style: const TextStyle(
        //     fontSize: 15,
        //   ),
        // ),
        Text(
          saldo,
          style: const TextStyle(
            fontSize: 20,
          ),
        ),
      ],
    );
  }

  Widget _seach() {
    return const TextField(
      style: TextStyle(color: Colors.white),
      decoration: InputDecoration(
          icon: Icon(
            Icons.search,
            color: Colors.white,
          ),
          hintText: "Buscar transaccion",
          hintStyle: TextStyle(color: Colors.white)),
    );
  }

  void _listTransaction() {
    _pref.then((pref) {
      var id = pref.getString("uid") ?? "";
      _transactionController.listAll(id).then((value) {
        setState(() {
          _lista = value;
          total = 0;
          for (var element in value) {
            if (element.type == "Ingreso") {
              total += element.amount!;
            }
            if (element.type == "Gasto") {
              total -= element.amount!;
            }
          }
        });
      });
    });
  }

  // void _totalAmount() async {
  //   String id = "";
  //   _pref.then((pref) {
  //     setState(() {
  //       id = pref.getString("uid") ?? "";
  //     });
  //   });

  //   var lista = await _transactionController.listAll(id);
  //   for (var element in lista) {
  //     if (element.type == "Ingreso") {
  //       total += element.amount!;
  //     }
  //     if (element.type == "Gasto") {
  //       total -= element.amount!;
  //     }
  //   }
  // }
}

import 'package:flutter/material.dart';

class EditTransactionPage extends StatelessWidget {
  const EditTransactionPage({super.key});
  final String fecha = "14/11/2022";
  final String title = "Ingreso por sueldo";
  final int value = 100000;
  final String category = "Comida";
  final String type = "Gasto";
  final String description = "Ingreso por saldo de trabajo";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Editar Transacción"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: _form(context),
        ),
      ),
    );
  }

  Widget _form(context) {
    final formkey = GlobalKey<FormState>();

    return Form(
      key: formkey,
      child: Column(
        children: [
          const SizedBox(height: 15),
          _fieldTitle(title),
          const SizedBox(height: 10),
          _fieldValue(value),
          const SizedBox(height: 10),
          _fieldType(type),
          const SizedBox(height: 10),
          _fieldDate(fecha),
          const SizedBox(height: 10),
          _fielCategory(category),
          const SizedBox(height: 10),
          _fieldDescription(description),
          const SizedBox(height: 10),
          _fieldAddPhoto(),
          const SizedBox(height: 40),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Theme.of(context)
                      .appBarTheme
                      .backgroundColor, // Background color
                ),
                child: const Text("Guardar"),
                onPressed: () {},
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Theme.of(context)
                      .appBarTheme
                      .backgroundColor, // Background color
                ),
                child: const Text("Eliminar"),
                onPressed: () {},
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _fieldTitle(String title) {
    return TextFormField(
      //autofocus: true,
      maxLength: 20,
      initialValue: title,
      keyboardType: TextInputType.name,
      decoration: const InputDecoration(
        //icon: Icon(Icons.title),
        border: OutlineInputBorder(),
        labelText: 'Titulo',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El campo es obligatorio";
        }
        return null;
      },
      onSaved: (value) {},
    );
  }

  Widget _fieldValue(int value) {
    return TextFormField(
      initialValue: "$value",
      textAlign: TextAlign.right,
      keyboardType: TextInputType.number,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Monto',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El campo es obligatorio";
        }
        if (int.parse(value) < 1) {
          return "El valor no es valido";
        }
        return null;
      },
      onSaved: (value) {},
    );
  }

  Widget _fieldType(String type) {
    var opciones = <String>["Ingreso", "Gasto"];
    return DropdownButtonFormField(
      value: type,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Tipo',
      ),
      items: opciones
          .map<DropdownMenuItem<String>>(
              (String value) => DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  ))
          .toList(),
      onChanged: (value) {},
    );
  }

  Widget _fieldDate(String fecha) {
    return Row(
      children: [
        IconButton(
          icon: const Icon(Icons.date_range),
          onPressed: () {},
        ),
        Expanded(
          child: TextFormField(
            initialValue: fecha,
            keyboardType: TextInputType.datetime,
            decoration: const InputDecoration(
              //icon: Icon(Icons.title),
              border: OutlineInputBorder(),
              labelText: "Fecha",
            ),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return "El campo es obligatorio";
              }
              return null;
            },
            onSaved: (value) {},
          ),
        ),
      ],
    );
  }

  Widget _fielCategory(String category) {
    var opciones = <String>["Servicios", "Sueldo", "Comida", "Entretenimiento"];
    return DropdownButtonFormField(
      value: category,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Categoria',
      ),
      items: opciones
          .map<DropdownMenuItem<String>>(
              (String value) => DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  ))
          .toList(),
      onChanged: (value) {},
    );
  }

  Widget _fieldDescription(String description) {
    return TextFormField(
      initialValue: description,
      keyboardType: TextInputType.text,
      decoration: const InputDecoration(
        //icon: Icon(Icons.title),
        border: OutlineInputBorder(),
        labelText: 'Descripcion',
      ),
      onSaved: (value) {},
    );
  }

  Widget _fieldAddPhoto() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Divider(
          height: 20,
          thickness: 2,
        ),
        const Text(
          "Agregar Fotografia",
          style: TextStyle(
            fontSize: 14,
            color: Colors.grey,
          ),
          textAlign: TextAlign.center,
        ),
        IconButton(
          icon: const Icon(
            Icons.add_a_photo,
            size: 40,
          ),
          onPressed: () {},
        ),
      ],
    );
  }
}

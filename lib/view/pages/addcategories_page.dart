import 'package:flutter/material.dart';

class AddCategoriesPage extends StatelessWidget {
  const AddCategoriesPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Agregar Categoria"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: _form(context),
        ),
      ),
    );
  }

  Widget _form(context) {
    final formkey = GlobalKey<FormState>();

    return Form(
      key: formkey,
      child: Column(
        children: [
          const SizedBox(height: 15),
          _fieldName(),
          const SizedBox(height: 10),
          _fieldSubCategory(),
          const SizedBox(height: 10),
          _fieldDescription(),
          const SizedBox(height: 10),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: Theme.of(context)
                  .appBarTheme
                  .backgroundColor, // Background color
            ),
            child: const Text("Guardar"),
            onPressed: () {},
          ),
        ],
      ),
    );
  }

  Widget _fieldName() {
    return TextFormField(
      //autofocus: true,
      maxLength: 20,
      keyboardType: TextInputType.name,
      decoration: const InputDecoration(
        //icon: Icon(Icons.title),
        border: OutlineInputBorder(),
        labelText: 'Ingresar Nombre',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El campo es obligatorio";
        }
        return null;
      },
      onSaved: (value) {},
    );
  }

  Widget _fieldSubCategory() {
    return TextFormField(
      //autofocus: true,
      maxLength: 20,
      keyboardType: TextInputType.name,
      decoration: const InputDecoration(
        //icon: Icon(Icons.title),
        border: OutlineInputBorder(),
        labelText: 'Ingresar subcategoria',
      ),
      onSaved: (value) {},
    );
  }

  Widget _fieldDescription() {
    return TextFormField(
      keyboardType: TextInputType.text,
      decoration: const InputDecoration(
        //icon: Icon(Icons.title),
        border: OutlineInputBorder(),
        labelText: 'Descripcion',
      ),
      onSaved: (value) {},
    );
  }
}

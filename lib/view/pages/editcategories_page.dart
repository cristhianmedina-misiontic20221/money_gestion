import 'package:flutter/material.dart';

class EditCategoryPage extends StatelessWidget {
  const EditCategoryPage({super.key});

  final String name = "Sueldo";
  final String subCategory = "Sueldo trabajo 1";
  final String description = "Ingreso por sueldo de trabajo 1";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Editar Categoria"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: _form(context),
        ),
      ),
    );
  }

  Widget _form(context) {
    final formkey = GlobalKey<FormState>();

    return Form(
      key: formkey,
      child: Column(
        children: [
          const SizedBox(height: 15),
          _fieldName(name),
          const SizedBox(height: 10),
          _fieldSubCategory(subCategory),
          const SizedBox(height: 10),
          _fieldDescription(description),
          const SizedBox(height: 40),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Theme.of(context)
                      .appBarTheme
                      .backgroundColor, // Background color
                ),
                child: const Text("Editar"),
                onPressed: () {},
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Theme.of(context)
                      .appBarTheme
                      .backgroundColor, // Background color
                ),
                child: const Text("Eliminar"),
                onPressed: () {},
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _fieldName(String name) {
    return TextFormField(
      //autofocus: true,
      initialValue: name,
      maxLength: 20,
      keyboardType: TextInputType.name,
      decoration: const InputDecoration(
        //icon: Icon(Icons.title),
        border: OutlineInputBorder(),
        labelText: 'Ingresar Nombre',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El campo es obligatorio";
        }
        return null;
      },
      onSaved: (value) {},
    );
  }

  Widget _fieldSubCategory(String subCategory) {
    return TextFormField(
      //autofocus: true,
      initialValue: subCategory,
      maxLength: 20,
      keyboardType: TextInputType.name,
      decoration: const InputDecoration(
        //icon: Icon(Icons.title),
        border: OutlineInputBorder(),
        labelText: 'Ingresar subcategoria',
      ),
      onSaved: (value) {},
    );
  }

  Widget _fieldDescription(String description) {
    return TextFormField(
      initialValue: description,
      keyboardType: TextInputType.text,
      decoration: const InputDecoration(
        //icon: Icon(Icons.title),
        border: OutlineInputBorder(),
        labelText: 'Descripcion',
      ),
      onSaved: (value) {},
    );
  }
}

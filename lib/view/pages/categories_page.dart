import 'package:flutter/material.dart';
import 'package:money_gestion/view/pages/editcategories_page.dart';

import '../widgets/drawer.dart';
import 'addcategories_page.dart';

class CategoriesPage extends StatelessWidget {
  const CategoriesPage({super.key});

  @override
  Widget build(BuildContext context) {
    final listaC = _toListCategories();

    return WillPopScope(
      onWillPop: () async {
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: _title(),
          centerTitle: true,
        ),
        drawer: const DrawerWitdget(),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: ListView.builder(
                  itemCount: listaC.length,
                  itemBuilder: (context, index) => ListTile(
                    leading: CircleAvatar(
                      child: Text(listaC[index][0].toUpperCase()),
                    ),
                    title: Text(listaC[index]),
                    subtitle: const Text("Descripcion"),
                    onTap: () {
                      // Ir a la ventana de cobro
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const EditCategoryPage(),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
          child: const Icon(Icons.add),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const AddCategoriesPage(),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _title() {
    return const Text(
      "Categorias",
      style: TextStyle(
        fontSize: 20,
      ),
    );
  }

  List<String> _toListCategories() {
    //Traer la lista de cobros del dia de la BD
    return List<String>.generate(
      5,
      (index) => "Categoria ${index + 1}",
    );
  }
}

import 'package:flutter/material.dart';

import '../../controller/login.dart';
import '../../controller/request/register.dart';

// ignore: must_be_immutable
class RegisterPage extends StatelessWidget {
  final password = TextEditingController();
  final cpassword = TextEditingController();

  late RegisterRequest _data;
  late LoginController _controller;

  RegisterPage({super.key}) {
    _data = RegisterRequest();
    _controller = LoginController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(30.0),
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              children: [
                _text(),
                const SizedBox(height: 20),
                _form(context),
                const SizedBox(height: 20),
                _login(context),
              ],
            ),
          ),
        ),
      ),
      backgroundColor: Colors.lightBlue[50],
    );
  }

  Widget _text() {
    return Padding(
      padding: const EdgeInsets.only(top: 30.0),
      child: Row(
        children: const [
          Text(
            "Registrarse",
            style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              fontFamily: 'Arial Black',
              //color: Color.fromARGB(255, 32, 137, 223),
            ),
          ),
        ],
      ),
    );
  }

  Widget _login(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text("Tienes cuenta?"),
        TextButton(
          child: const Text("Ingresar"),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ],
    );
  }

  Widget _form(BuildContext context) {
    var formKey = GlobalKey<FormState>();

    return Form(
      key: formKey,
      child: Column(
        children: [
          _basicWidget(
            "Nombre",
            validateRequiredFiel,
            (newValue) {
              _data.name = newValue!;
            },
          ),
          _basicWidget(
            "Email",
            validateRequiredFiel,
            (newValue) {
              _data.email = newValue!;
            },
          ),
          _basicWidget(
            "Contraseña",
            validateRequiredFiel,
            (newValue) {
              _data.password = newValue!;
            },
            controller: password,
            isPassword: true,
          ),
          _basicWidget(
            "Confirmar contraseña",
            validateRequiredFiel,
            (newValue) {},
            controller: cpassword,
            isPassword: true,
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: Theme.of(context)
                  .appBarTheme
                  .backgroundColor, // Background color
            ),
            child: const Text(
              "Registrar",
              style: TextStyle(fontSize: 18),
            ),
            onPressed: () async {
              if (formKey.currentState!.validate()) {
                String pass = password.text;
                String cpass = cpassword.text;

                if (pass == cpass) {
                  formKey.currentState!.save();
                  try {
                    final mess = ScaffoldMessenger.of(context);
                    final nav = Navigator.of(context);
                    await _controller.registerNewUser(_data);

                    mess.showSnackBar(
                      const SnackBar(
                          content: Text("Usuario registrado correctamente")),
                    );

                    nav.pop();
                  } catch (error) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text(error.toString()),
                      ),
                    );
                  }
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Contraseñas no coiciden')));
                }
              }
            },
          ),
        ],
      ),
    );
  }

  Widget _basicWidget(String title, FormFieldValidator<String?> validate,
      FormFieldSetter<String?> save,
      {TextEditingController? controller, bool isPassword = false}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: TextFormField(
        maxLength: 50,
        obscureText: isPassword,
        keyboardType: TextInputType.emailAddress,
        controller: controller,
        decoration: InputDecoration(
          border: const OutlineInputBorder(),
          labelText: title,
        ),
        validator: validate,
        onSaved: save,
      ),
    );
  }

  String? validateRequiredFiel(String? value) {
    if (value == null || value.isEmpty) {
      return "El campo es obligatorio";
    }
    return null;
  }
}

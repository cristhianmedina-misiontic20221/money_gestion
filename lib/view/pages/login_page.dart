import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../controller/login.dart';
import '../../controller/request/login.dart';
import '../icons/my_icons.dart';
import 'register_page.dart';
import 'transaction_page.dart';

// ignore: must_be_immutable
class LoginPage extends StatelessWidget {
  final _pref = SharedPreferences.getInstance();

  final _imageUrl = "assets/images/Logo.png";

  late LoginController _controller;
  late LoginRequest _request;

  LoginPage({super.key}) {
    _controller = LoginController();
    _request = LoginRequest();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(30.0),
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              children: [
                _logo(),
                _form(context),
                _register(context),
                _alternativeLogin(),
              ],
            ),
          ),
        ),
      ),
      backgroundColor: Colors.lightBlue[50],
    );
  }

  Widget _logo() {
    return Column(
      children: [
        const SizedBox(height: 20),
        Image.asset(
          _imageUrl,
          height: 200,
        ),
        const SizedBox(height: 70),
      ],
    );
  }

  Widget _register(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text("No tienes cuenta?"),
        TextButton(
          child: const Text("Registrate"),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => RegisterPage()),
            );
          },
        ),
      ],
    );
  }

  Widget _alternativeLogin() {
    return Column(
      children: [
        const Divider(
          height: 30,
          thickness: 2,
        ),
        const Text(
          "O Iniciar Sesion con",
          style: TextStyle(
            fontSize: 14,
            color: Colors.grey,
          ),
        ),
        const SizedBox(height: 8),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton.icon(
              label: const Text("Facebook"),
              icon: const Icon(MyIcons.facebook),
              onPressed: () {},
            ),
            ElevatedButton.icon(
              label: const Text("Google"),
              icon: const Icon(MyIcons.google),
              onPressed: () {},
              style: ElevatedButton.styleFrom(backgroundColor: Colors.red),
            ),
          ],
        ),
        const SizedBox(height: 16),
      ],
    );
  }

  Widget _form(BuildContext context) {
    final formKey = GlobalKey<FormState>();

    return Form(
      key: formKey,
      child: Column(
        children: [
          _fieldEmail(),
          const SizedBox(height: 8),
          _fieldPassword(),
          const SizedBox(height: 10),
          _buttonIniciarSesion(context, formKey)
        ],
      ),
    );
  }

  Widget _buttonIniciarSesion(
      BuildContext context, GlobalKey<FormState> formKey) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            Theme.of(context).appBarTheme.backgroundColor, // Background color
      ),
      child: const Text(
        "Iniciar Sesion",
        style: TextStyle(fontSize: 18),
      ),
      onPressed: () async {
        if (formKey.currentState!.validate()) {
          formKey.currentState!.save();

          //Validar correo y clave BD
          try {
            final nav = Navigator.of(context);
            var userInfo = await _controller.validateEmailPassword(_request);
            var pref = await _pref;
            pref.setString("uid", userInfo.id!);
            pref.setString("name", userInfo.name!);
            pref.setString("email", userInfo.email!);

            nav.push(
              MaterialPageRoute(
                builder: (context) => const TransactionPage(),
              ),
            );
          } catch (e) {
            // showDialog(
            //   context: context,
            //   builder: (context) => AlertDialog(
            //     title: const Text("Ventas"),
            //     content: Text(e.toString()),
            //   ),
            // );

            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(e.toString())));
          }
        }
      },
    );
  }

  Widget _fieldEmail() {
    return TextFormField(
      maxLength: 50,
      keyboardType: TextInputType.emailAddress,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Correo Electrónico',
        prefixIcon: Icon(Icons.email),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "el correo electronico es obligatorio";
        }
        if (!value.contains("@") || !value.contains(".")) {
          return "El correo tiene un formato invalido";
        }
        return null;
      },
      onSaved: (value) {
        _request.email = value!;
      },
    );
  }

  Widget _fieldPassword() {
    return TextFormField(
      maxLength: 30,
      obscureText: true,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Contraseña',
        prefixIcon: Icon(Icons.lock),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "La contraseña es obligatoria";
        }
        if (value.length < 6) {
          return "Minimo debe contener 6 caracteres";
        }
        return null;
      },
      onSaved: (value) {
        _request.password = value!;
      },
    );
  }
}

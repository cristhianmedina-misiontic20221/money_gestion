import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'view/pages/login_page.dart';
import 'view/pages/transaction_page.dart';

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _pref = SharedPreferences.getInstance();
  //Widget _init = LoginPage();

  Widget _init = const Scaffold(
    body: Center(
      child: CircularProgressIndicator(),
    ),
  );

  @override
  void initState() {
    super.initState();
    _pref.then((pref) {
      setState(() {
        if (pref.getString("uid") != null) {
          _init = const TransactionPage();
        } else {
          _init = LoginPage();
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Money Gestion",
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          color: Color.fromARGB(255, 5, 75, 180),
        ),
        scaffoldBackgroundColor: Colors.lightBlue[50],
        primaryColor: const Color.fromARGB(255, 5, 75, 180),
      ),
      home: _init,
    );
  }
}

import 'package:money_gestion/model/entity/user.dart';

import '../../model/repository/user.dart';
import '../model/repository/fb_auth.dart';
import 'request/login.dart';
import 'request/register.dart';
import 'response/userinfo.dart';

class LoginController {
  late final UserRepository _userRepository;
  late final FirebaseAuthenticationRepository _authRepository;

  LoginController() {
    _userRepository = UserRepository();
    _authRepository = FirebaseAuthenticationRepository();
  }

  Future<UserInfoResponse> validateEmailPassword(LoginRequest request) async {
    await _authRepository.signInEmailPassword(request.email, request.password);

    // Consultar el usuario que tenga el correo dado
    var user = await _userRepository.findByEmail(request.email);
    return UserInfoResponse(
      id: user.id,
      email: user.email,
      name: user.name,
    );
  }

  Future<void> registerNewUser(RegisterRequest request) async {
    //consulto si el usuario existe
    try {
      await _userRepository.findByEmail(request.email);
      return Future.error("Ya existe un usuario con este Email");
    } catch (e) {
      //No exite el correo en BD

      //Crear un correo/ clave en Fire Authentication
      await _authRepository.createEmailPasswordAccount(
          request.email, request.password);

      //Agregar informacion adicional en base de datos
      _userRepository.save(UserEntity(
        email: request.email,
        name: request.name,
      ));
    }
  }

  Future<void> logout() async {
    await _authRepository.signOut();
  }
}

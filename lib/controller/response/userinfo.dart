class UserInfoResponse {
  late String? id;
  late String? name;
  late String? email;

  UserInfoResponse({this.id, this.email, this.name});
}

import 'package:money_gestion/model/entity/transaction.dart';
import 'package:money_gestion/model/repository/transaction.dart';

class TransactionController {
  late TransactionRepository _repository;
  int total = 0;

  TransactionController() {
    _repository = TransactionRepository();
  }

  Future<void> save(TransactionEntity transaction) async {
    await _repository.addTransaction(transaction);
  }

  Future<List<TransactionEntity>> listAll(String id) async {
    return await _repository.getAllByUser(id);
  }
}

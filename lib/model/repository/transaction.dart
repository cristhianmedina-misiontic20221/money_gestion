import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:money_gestion/model/entity/transaction.dart';

class TransactionRepository {
  late final CollectionReference _collection;

  TransactionRepository() {
    _collection = FirebaseFirestore.instance.collection("transactions");
  }

  addTransaction(TransactionEntity transaction) async {
    await _collection
        .withConverter<TransactionEntity>(
            fromFirestore: TransactionEntity.fromFirestore,
            toFirestore: (value, options) => value.toFirestore())
        .add(transaction);
  }

  Future<List<TransactionEntity>> getAllByUser(String id) async {
    var query = await _collection
        .where("user", isEqualTo: id)
        .withConverter<TransactionEntity>(
            fromFirestore: TransactionEntity.fromFirestore,
            toFirestore: (value, options) => value.toFirestore())
        .get();

    var transactions = query.docs.cast().map<TransactionEntity>((e) {
      var transaction = e.data();
      transaction.id = e.id;
      return transaction;
    });

    return transactions.toList();
  }
}

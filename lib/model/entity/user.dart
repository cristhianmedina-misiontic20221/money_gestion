import 'package:cloud_firestore/cloud_firestore.dart';

class UserEntity {
  late String? id;
  late String? email;
  late String? name;

  UserEntity({
    this.email,
    this.name,
  });

  factory UserEntity.fromFirestore(
    DocumentSnapshot<Map<String, dynamic>> snapshot,
    SnapshotOptions? options,
  ) {
    var data = snapshot.data();

    return UserEntity(
      email: data?["email"],
      name: data?["name"],
    );
  }

  Map<String, dynamic> toFirestore() {
    return {
      if (name != null && name!.isNotEmpty) "name": name,
      if (email != null && email!.isNotEmpty) "email": email,
    };
  }

  @override
  String toString() {
    return "UserEntity {$name, $email}";
  }
}

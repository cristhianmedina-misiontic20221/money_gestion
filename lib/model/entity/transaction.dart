import 'package:cloud_firestore/cloud_firestore.dart';

class TransactionEntity {
  late String? id;
  late String? user;
  late String? title;
  late int? amount;
  late String? type;
  late String? date;
  late String? category;
  late String? description;

  TransactionEntity(
      {this.user,
      this.title,
      this.amount,
      this.type,
      this.date,
      this.category,
      this.description});

  factory TransactionEntity.fromFirestore(
    DocumentSnapshot<Map<String, dynamic>> snapshot,
    SnapshotOptions? options,
  ) {
    var data = snapshot.data();

    return TransactionEntity(
      user: data?["user"],
      title: data?["title"],
      amount: data?["amount"],
      type: data?["type"],
      date: data?["date"],
      category: data?["category"],
      description: data?["description"],
    );
  }

  Map<String, dynamic> toFirestore() {
    return {
      if (user != null && user!.isNotEmpty) "user": user,
      if (title != null && title!.isNotEmpty) "title": title,
      if (amount != null) "amount": amount,
      if (type != null && type!.isNotEmpty) "type": type,
      if (date != null && date!.isNotEmpty) "date": date,
      if (category != null && category!.isNotEmpty) "category": category,
      if (description != null && description!.isNotEmpty)
        "description": description,
    };
  }
}
